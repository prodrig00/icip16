% Template for ICIP-2013 paper; to be used with:
%          spconf.sty  - ICASSP/ICIP LaTeX style file, and
%          IEEEbib.bst - IEEE bibliography style file.
% --------------------------------------------------------------------------
\documentclass{article}
\usepackage{spconf,amsmath,graphicx}

% Example definitions.
% --------------------
\def\x{{\mathbf x}}
\def\L{{\cal L}}


% Example definitions.
% --------------------
\def\x{{\mathbf x}}
\def\L{{\cal L}}

\usepackage{verbatim}
\usepackage{ifpdf}
\usepackage{url}
\usepackage{xspace}
\usepackage{multirow}
\usepackage{float}
% \usepackage[ruled,vlined]{algorithm2e}
\usepackage{subfigure}

\usepackage{amsmath,amssymb,subfigure}
\usepackage[ruled,vlined]{algorithm2e}

\usepackage{bm}

\usepackage{xcolor}        
\usepackage{color}
\usepackage{setspace}
\usepackage{tabu}

\ifpdf
  \usepackage{graphicx}
  \DeclareGraphicsExtensions{.pdf,.jpg}
  \pdfcompresslevel=9
\else
  \usepackage{graphicx}
  \DeclareGraphicsExtensions{.pstex,.ps,.eps,.jpg}
\fi
\usepackage{color}

\newcommand{\cmnt}[1]{%
{\color{red} \tt #1}%
}%

\newcommand{\ie}{{i.e.}\xspace}
\newcommand{\eg}{{e.g.}\xspace}
\newcommand{\etal}{{et al.}\xspace}

\newcommand{\ran}{\mathop{\mathrm{ran}}}
\newcommand{\diag}{\mathop{\mathrm{diag}}}
\newcommand{\rank}{\mathrm{rank}}
\newcommand{\sign}{\mathop{\mathrm{sign}}}

\newcommand{\doi}[1]{\textsc{doi:} \textsf{#1}\xspace}

\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator{\shrink}{shrink}


\floatstyle{plain}
\newfloat{algorithm}{tbp}{loa}
\floatname{algorithm}{Algorithm}


\DeclareMathOperator{\partialSVD}{partialSVD}
\DeclareMathOperator{\incrementalSVD}{incSVD}
\DeclareMathOperator{\replaceSVD}{repSVD}
\DeclareMathOperator{\downdateSVD}{dwnSVD}
\DeclareMathOperator{\svd}{SVD}

\DeclareMathOperator{\batchPCP}{batchPCP}
\DeclareMathOperator{\medianFilter1D}{medFilt1D}

\DeclareMathOperator{\QR}{QR}
\DeclareMathOperator{\thinQR}{thinQR}

\newcommand{\mynote}[1]{%
{\color{red} \tt #1}%
}%



%%
%% LA-UR-16-20495
%%



% Title.
% ------
\title{Ghosting Suppression for Incremental Principal Component Pursuit Algorithms}
%

\twoauthors
  {Paul Rodr\'{i}guez}
%   \sthanks{This research was supported by the ``Fondo para la Innovaci\'on, la Ciencia y la Tecnolog\'ia'' (Fincyt) Program}}
	{Department of Electrical Engineering\\
	Pontificia Universidad Cat\'olica del Per\'u\\
	Lima, Peru}
  {Brendt Wohlberg
% \sthanks{This research was supported by the NNSA's Laboratory
%   Directed Research and Development Program.}
}
       {T-5 Applied Mathematics and Plasma Physics\\
        Los Alamos National Laboratory\\
        Los Alamos, NM 87545, USA}

%
% For example:
% ------------
%\address{School\\
%	Department\\
%	Address}
%
% Two addresses (uncomment and modify for two-address case).
% ----------------------------------------------------------
%\twoauthors
%  {A. Author-one, B. Author-two\sthanks{Thanks to XYZ agency for funding.}}
%	{School A-B\\
%	Department A-B\\
%	Address A-B}
%  {C. Author-three, D. Author-four\sthanks{The fourth author performed the work
%	while at ...}}
%	{School C-D\\
%	Department C-D\\
%	Address C-D}
%
\begin{document}
\ninept
%
\maketitle
%
\begin{abstract}
In video background modeling, ghosting occurs when an object that belongs to the
background is assigned to the foreground. In the context of Principal
Component Pursuit, this usually
occurs when a moving object occludes a high contrast
background object, a moving object suddenly stops, or a stationary object
suddenly starts moving.

Based on a previously developed incremental PCP method, 
% for which the
% background estimate represents the background as observed for the last
% $L$ frames, 
we propose a novel algorithm that uses two simultaneous background estimates based on observations over the previous
$n_1$ and $n_2$ ($n_1 \ll n_2$) frames in order to identify and 
 diminish the ghosting effect. % in almost real-time -- something like that
% Our computational results show that our proposed
% method has a high accuracy (F-measure) and outperforms
% other incremental / ``on-line'' PCP-like methods. 
Our computational results show that the proposed method greatly improves 
both the subjective quality and accuracy as determined by the F-measure.

% to the best of our knowledge, 
% the proposed method is a novel approach, even for the class of incremental / ``on-line''
% PCP-like algorithms.

\end{abstract}

%
\begin{keywords}
Incremental Principal Component Pursuit, Ghosting
\end{keywords}
%
\section{Introduction}
\label{sec:intro}

Video background modeling consists of segmenting the moving
objects or ``foreground'' from the static ``background''. 
Ghosting occurs when the 
foreground estimate includes 
% non-existing \cmnt{this could be more clearly phrased} moving objects; this effect could include
phantoms or smear replicas from actual moving objects, or from objects that really 
belong to the background. An example of ghosting is shown in Fig.~\ref{fig:ghost-ex}.
Ghosting not only visually degrades the foreground estimation, but also
has a negative impact when estimating a binary foreground mask, which is 
a key pre-processing task for target tracking, recognition and behavior analysis
in digital videos.

The Principal Component Pursuit (PCP) method is currently considered to be one 
of the leading algorithms for video background modeling~\cite{bouwmans-2014-robust}. 
Although the PCP method is inherently a batch method (for a
complete list of algorithms, see \cite{bouwmans-2014-robust}) in that
a large number of frames have to be observed before starting any processing, there 
exits a handful of incremental/on-line alternatives 
\cite{qiu-2011-support, he-2012-incremental, seidel-2014-prost, xu-2013-gosus, rodriguez-2015-incremental}, 
for  which the processing is performed one frame at a time. 
While these alternatives have some
advantages over their batch counterparts, mainly their low computational cost and
memory footprint, which could allow real-time processing of live-feed videos, 
they exhibit ghosting effects that are not usually observed in 
the PCP batch methods.


In this paper, we exploit a feature that is common in the 
incremental/on-line
% \cmnt{I would refer to it this way once, and then use just one of the terms thereafter, perhaps after discussing the issue.} 
PCP-like algorithms\footnote{Algorithms \cite{qiu-2011-support, he-2012-incremental, seidel-2014-prost, xu-2013-gosus, rodriguez-2015-incremental}
use different terms to refer to the same general property; in this paper, from this point onwards, we choose to use the
term ``incremental''.}: the current background estimate is the result 
of observing a limited number of past frames. While the particular details of 
how this is handled by
\cite{qiu-2011-support, he-2012-incremental, seidel-2014-prost, xu-2013-gosus, rodriguez-2015-incremental}
varies, this feature allows these type of algorithms to adapt to changes in the background on the fly.
The key and novel idea of the proposed  ghosting suppression method is to simultaneously use 
two background estimates derived from the previous
$n_1$ and $n_2$ frames respectively ($n_1 \ll n_2$) to reduce the ghosting effect.
% 

Our computational results, which are carried out with
the incPCP algorithm~\cite{rodriguez-2015-incremental}, 
show that our proposed algorithm 
efficaciously diminishes the ghosting effect observed in the 
foreground estimate
and at the same time attains better accuracy (F-measure) than other 
incremental PCP algorithms
when estimating a binary foreground mask, even when the mask 
is computed via a simple global thresholding per frame.

% any other incremental / ``online'' PCP-like algorithm could benefit from 
% the key idea ...

% \cite{rodriguez-2013-fast}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Previous related work}
\label{sec:related}

\subsection{Ghost removal methods}
\label{sec:gMethods}

Ghosting removal methods have attracted some attention over the past few years;
in this section we give a brief overview of several works
\cite{yin-2008-time, suo-2008-improved, yang-2010-adaptive, chuan-2012-ghosts, 
wang-2014-fast, yang-2015-pixel, oh-2015-partial} 
that are based on different video background modeling approaches and
that explicitly target the ghosting problem.


In the context of Gaussian Mixture Models (GMM), \cite{chuan-2012-ghosts} proposed a method to detect ghosts 
and stationary foreground by dual-direction (observing ``past'' and ``future'' frames) background modeling. 
Instead, \cite{suo-2008-improved}
incorporated an adaptive parameter adjustment to GMM. In both cases, superior results 
are obtained when compared to the traditional GMM.


Pixel-level methods, for which a model of the recent history is
built for each pixel location, are also popular alternatives for video background modeling. In this 
context, \cite{wang-2014-fast} estimated the relevance
% efficacies \cmnt{not sure what this means here} 
of the pixel's historical
background values, selectively adapting to background changes with different timescales and thus 
mitigating the ghosting effect. %\cmnt{previous sentence not so clear} 
Instead, in \cite{yang-2015-pixel} context features for each pixel
were compressively sensed from local patches, and the background model was
renewed in order to handle the ghosting effect. Adaptive median filtering and background update, based on the  
motion information, was used in~\cite{yang-2010-adaptive} to remove the ghosting effect. 
% \cmnt{these descriptions are so brief that they don't really give enough detail to understand 
% the ideas underlying these methods. It would be nice to explain in more detail if space were available, 
% but since it isn't perhaps save some space by just mentioning something like ``[11-13] address ghosting 
% removal for pixel-level background modeling methods''? An alternative approach would be to group the prior 
% methods into general classes of methods and then just describe each class when citing all methods within 
% that class. This might also save space.}


In the context of batch PCP-based methods, \cite{oh-2015-partial} proposed to 
minimize the partial sum of singular values, in place of the nuclear norm (see (\ref{equ:pcp-original}));
among other effects of this modification to the original problem, the authors claim that it delivers
a ghost-free sparse representation.


% general 

In a more general context, \cite{yin-2008-time} presented a fast and
effective algorithm for ghost detection and removal, using edge
comparison for ghost detection and removal during tracking. It is
claimed that this method can be integrated into any video background
modeling method that estimates a difference map between the current
frame and a background estimates.

% median filtering 

% \cite{yang-2010-adaptive} proposed an adaptive background method to robustly track left objects and 
% to effectively remove ghosts. The proposed method is based on background 
% subtraction using adaptive median filtering and background update using 
% motion information.

% \cite{yin-2008-real} focus on detection of ghosts caused by the starting
% or stopping of objects. We propose a fast and effective method for ghost 
% detection that compares the
% similarity between the edges of the detected
% foreground objects and those of the current frame
% based on object-level knowledge of moving objects.

% ------------------------


% GMM
% \cite{chuan-2012-ghosts} proposed a method to detect ghosts and stationary foreground by 
% dual-direction background modeling. The forward background model and the backward background 
% model are built by GMM and a simple regression model respectively.

% \cite{suo-2008-improved} a background modeling algorithm based on adaptive parameter 
% adjustment of the Mixture Gaussian is proposed. Superior results compared to  the traditional GMM.

% ------------------------

% pixel level
% \cite{wang-2014-fast} a fast pixel-level adapting background detection algorithm is presented.
% The background model records not only each pixel's historical
% background values, but also estimates the efficacies of
% these values, based on the occurrence statistics, selectively adapting to
% background changes with different timescales, and
% restraining the generation of ghosts. 

% \cite{yang-2015-pixel} models the background with a set of context features for each pixel, 
% which are compressively sensed from local patches, called Pixel-to-Model (P2M). the neighboring 
% background model will be 
% renewed according to the maximum P2M distance in order to handle ghost holes.

% ------------------------

% % PCP based
% \cite{oh-2015-partial} PCP-based.  instead of minimizing the nuclear norm, we propose to 
% minimize the partial sum of singular values, which implicitly encourages the target rank 
% constraint in rank minimization. Claims ghost-free sparse representation.

% ------------------------


Finally we mention that there are several PCP-based methods (some of them
incremental), see for instance \cite{zhou-2013-moving, hu-2015-online}
among others, that usually include an extra foreground contiguity term
which is used to directly estimate a binary foreground mask. However, such methods
do not explicitly target the elimination of the ghost effect in the sparse component. 


\subsection{Incremental PCP methods}
\label{sec:alternative}


To the best of our knowledge, recursive projected compressive sensing
(ReProCS) \cite{qiu-2011-support,guo-2014-online}
along with Grassmannian robust adaptive subspace tracking algorithm
(GRASTA) \cite{he-2012-incremental}, $\ell_p$-norm robust online
subspace tracking (pROST) \cite{seidel-2014-prost}, Grassmannian
online subspace updates with structured sparsity (GOSUS)
\cite{xu-2013-gosus} and the incremental PCP (incPCP) \cite{rodriguez-2015-incremental} 
are the only PCP-like methods for the video
background modeling problem that are considered to be
incremental. However, except for incPCP,
these methods have a batch
initialization/training stage as the default/recommended initial
background estimate\footnote{GRASTA and GOSUS can perform
  the initial background estimation in a non-batch fashion, however
  the resulting performance is not as good as when the default batch
  procedure is used; see  \cite[Section 6]{rodriguez-2015-incremental}.
  pROST is closely related to GRASTA, and it shares the same restrictions.
  All variants of ReProCS also use a batch initialization stage.}.

\begin{figure}[ht]
\centering
  \begin{tabular}{cc}
%
    \hspace{-0.5mm}\subfigure[\protect\rule{0pt}{1.5em}Video V640: Frame 50.]
    {\includegraphics[width=4.0cm]{lank_50}} &
% % % % %
    \hspace{-0.5mm}\subfigure[\protect\rule{0pt}{1.5em}Video V640: Frame 310.]
    {\includegraphics[width=4.0cm]{lank_310}} \\
%
    \hspace{-2mm}\subfigure[\protect\rule{0pt}{1.5em}incPCP sparse component.]
    {\includegraphics[width=4.0cm]{incPCP_std_lank50}} &
%
    \hspace{-2mm}\subfigure[\protect\rule{0pt}{1.5em}incPCP sparse component.]
    {\includegraphics[width=4.0cm]{incPCP_std_lank310}}     \\
% 
    \hspace{-2mm}\subfigure[\protect\rule{0pt}{1.5em}GRASTA sparse component.]
    {\includegraphics[width=4.0cm]{grasta_lank50}} &
%
    \hspace{-2mm}\subfigure[\protect\rule{0pt}{1.5em}GRASTA sparse component.]
    {\includegraphics[width=4.0cm]{grasta_lank310}}     

  \end{tabular}
%
%   \vspace{-5mm}
  \caption[LOF]{Original frames and corresponding sparse components for video V640\footnotemark[3]
  when analyzed with 
  the incPCP\footnotemark[4] and GRASTA\footnotemark[5] 
%   and  GOSUS\footnotemark[6] 
  algorithms. The ghosting effect is mainly noticeable in the 
  upper left corner, where some vehicles are stopped for a while between frames 50 and 310.}
 \label{fig:ghost-ex}
%
\end{figure}

\footnotetext[3]{A $640 \times 480$ pixel, 400-frame color video sequence
 at 15 fps, from the Lankershim Boulevard traffic
  surveillance dataset \cite[cam. 3]{lankershim-2007-dataset}}
\footnotetext[4]{Matlab code is publicly available~\cite{fastPCP-sim}.}
\footnotetext[5]{Publicly available Matlab code~\cite{grasta-sim} can only process grayscale videos.}
% \footnotetext[6]{Matlab code is publicly available~\cite{gosus-sim}.}
  
  
\subsection{Intuitive description of the incPCP algorithm}
\label{sec:incPCP}

Since the proposed ghosting suppression method is implemented via
the incPCP algorithm \cite{rodriguez-2014-matlab, rodriguez-2014-video, rodriguez-2015-incremental,
rodriguez-2015-jitter, silva-2015-jitter}, 
here we give a succinct description of this algorithm.
%  
We first recall that the PCP
problem 
\begin{equation}
\argmin_{L,S} 
 \| L \|_* + \lambda \| S \|_1  \;\; \text{ s.t. } \;\; D = L+S
\label{equ:pcp-original} 
%   
\end{equation}

\vspace{-1.0mm}
{\noindent}is derived as the
convex relaxation of the original problem~\cite[Section 2]{wright-2009-robust}
\vspace{-2.5mm}

% 
\begin{equation}
\argmin_{L,S}
 \;\; \rank( L ) + \lambda \| S \|_0  \; \text{ s.t. } D = L+S \;,
\label{equ:pcp-original0}
\end{equation}

% \vspace{-1.0mm}
{\noindent}based on decomposing matrix $D$ such that $D = L + S$, with low-rank
$L$ (background) and sparse $S$ (foreground).
%
While most PCP algorithms, including the Augmented
 Lagrange Multiplier (ALM) and inexact ALM (iALM) algorithms
\cite{liu-2010-robust,lin-2011-augmented}  are directly
based on~(\ref{equ:pcp-original}), this is not the only possible
tractable problem that can be derived
from~(\ref{equ:pcp-original0}). In particular, changing the
constraint $D = L + S$ to a penalty, the rank penalty to an
inequality constraint, relaxing the
$\ell_0$ norm by an $\ell_1$ norm, leads to the problem
\begin{equation}
\label{equ:amFastPCP-v1}
%
  \argmin_{L,S} \frac{1}{2} \| L + S - D \|_F^2 +  \lambda \| S \|_1
  %\text{ s.t. } \| L \|_* \leq t
  \; \text{ s.t. } \rank(L) \leq r \;.
\end{equation}
 
\vspace{-1mm}
{\noindent}A computationally efficient solution can be found via an alternating
optimization (AO) \cite{bezdek-2002-some} 
procedure, since it seems
natural to split (\ref{equ:amFastPCP-v1}) into a low-rank
approximation, i.e. $\argmin_{L} \allowbreak \frac{1}{2} \| L + S - D \|_F^2 \;
\text{ s.t. } \rank(L) \leq r$, with fixed $S$, followed by a
shrinkage, i.e. $\argmin_{S} \frac{1}{2} \| L + S - D \|_F^2 + \lambda
\| S \|_1$, with fixed $L$ computed in the previous iteration. 
% It is worth mentioning that 
The solution obtained via the previously described
AO procedure is of comparable
quality to the solution of the original PCP problem
(see \cite{rodriguez-2013-fast} for details),
%
being approximately an order of magnitude faster than the
iALM \cite{lin-2011-augmented} algorithm to construct a sparse
component of similar quality.


Furthermore, the low-rank approximation sub-problem can be solved via a
computationally efficient incremental procedure, based
on rank-1 modifications for thin SVD
(\cite{brand-2006-fast} and references therein),
and thus (\ref{equ:amFastPCP-v1}) can also be easily solved incrementally,
since the shrinkage step can be trivially computed in an
incremental fashion. 

In~\cite{rodriguez-2015-incremental} it was shown that the resulting
algorithm, called incPCP, is a
fully incremental PCP algorithm  for video
background modeling, which is able to processes one frame at a time,
obtaining similar results to batch PCP algorithms, while being able to
adapt to changes in the background. Furthermore, in \cite{rodriguez-2015-incremental}
 extensive computational comparisons with state-of-the-art methods were also given.
 


\section{Proposed algorithm}
\label{sec:gAlgo}
 
In this section we first describe the general ideas for our propose ghost removal 
method, that could be adapted to any incremental PCP algorithm, and then proceed to
give the particular adaptation of such ideas for the incPCP algorithm \cite{rodriguez-2015-incremental}.
 
Given an incremental PCP algorithm, for any frame $k$, the low-rank ($\mathbf{l}$) and 
sparse ($\mathbf{s}$) components 
satisfy
\begin{equation}
\label{equ:incPCP-g1}
%
\mathbf{d}_k \approx \mathbf{l}_k^{(n)} + \mathbf{s}_k^{(n)},
\end{equation}
%  
where $\mathbf{d}_k$ is the current frame from the observed video; in  (\ref{equ:incPCP-g1}) we use 
the super-script $n$ to differentiate low-rank and sparse components that have 
been obtained using the information of the past $n$ frames. It is also worth recalling that
in the context of PCP, once the low-rank component is available, the sparse component is computed via
the soft-thresholding shrinkage
\begin{equation}
\label{equ:incPCP-g2}
  \mathbf{s}_k^{(n)} = \shrink(\mathbf{d}_k - \mathbf{l}_k^{(n)}, \lambda),
\end{equation}
% 
where $\shrink(x, \lambda) = \sign(x) \max \{0, |x| - \lambda \}$.


Two simultaneous low-rank components, $\mathbf{l}_k^{(n_1)}$ and $\mathbf{l}_k^{(n_2)}$,
with $n_1 \ll n_2$ will be different if a video event's interpretation
differs over a longer/shorter time frame observation\footnote[6]{Here we are ruling out dramatic
changes in the background, such when the camera is moved, when a sudden illumination 
change occurs, etc.}: a typical example (albeit not the only one) would occur when an object
that is considered ``background'' for a short time frame observation, is identified as a mobile 
object that swiftly
% suddenly 
stops or as a stationary object that swiftly
% suddenly 
starts moving when a larger  time frame observation
is considered.

% ; likewise, a stationary object that becomes mobile at the very last part of a large time frame 
% observation will be understood as such, whereas for a short time frame observation, it could be considered as
% just a moving object (since it being stopped was never observed \cmnt{not so clear}). 

Furthermore, the sparse components, $\mathbf{s}_k^{(n_1)}$ and $\mathbf{s}_k^{(n_2)}$, will reflect the 
above mentioned differences as ghosts, which would be more or less prominent depending on the level 
of intensity of the non-background objects that do appear on the low-rank component; this is depicted in
Fig. \ref{fig:lowrank-diff}.


\begin{figure}[ht]
\centering
  \begin{tabular}{cc}
%
    \hspace{-0.5mm}\subfigure[\protect\rule{0pt}{1.5em}Frame 310 (V640 video) low-rank component when $n_1 = 20$.]
    {\includegraphics[width=4.0cm]{incPCP_Lw20_lank310}} &
% % % % %
    \hspace{-0.5mm}\subfigure[\protect\rule{0pt}{1.5em}Frame 310 (V640 video) low-rank component when $n_2 = 200$.]
    {\includegraphics[width=4.0cm]{incPCP_Lw200_lank310}} \\
% 
    \hspace{-0.5mm}\subfigure[\protect\rule{0pt}{1.5em}Frame 310 (V640 video) sparse component when $n_1 = 20$.]
    {\includegraphics[width=4.0cm]{incPCP_Sw20_lank310}} &
% % % % %
    \hspace{-0.5mm}\subfigure[\protect\rule{0pt}{1.5em}Frame 310 (V640 video) sparse component when $n_2 = 200$.]
    {\includegraphics[width=4.0cm]{incPCP_Sw200_lank310}} 
  \end{tabular}
%
  \vspace{-3mm}
  \caption{Two sets of low-rank / sparse components of frame 310, from the V640 test video, are shown. (a), (c)
  and (b), (d)
  are derived from the previous 20 and 200 frames respectively. Differences and ghosts are mainly observed in the upper left corner
%   where previously stopped objects still appear with different intensities 
  (see also Fig. \ref{fig:ghost-ex}).}
 \label{fig:lowrank-diff}
%
\end{figure}


% Taking into account the previous observation, 
If a foreground binary mask is estimated from each sparse component, i.e. $\mathbf{m}_k^{(n_1)}$ and $\mathbf{m}_k^{(n_2)}$,
these masks will include the moving objects as well as ghosts. However, as can be surmised from 
% \cmnt{not sure this is right here. perhaps ``seen in''?} 
Fig. \ref{fig:lowrank-diff},
the intersection of these binary masks will, with high confidence, only include the moving objects; likewise, 
$\mathbf{\cal B}_k = \sim\!\mathbf{m}_k^{(n_1)} \cup \sim\!\mathbf{m}_k^{(n_2)}$, the union of the masks complement,
will include %have value one for 
all the pixels of the background that are not occluded by a moving object.

Provided that the previous statement is true, then $\mathbf{\cal B}_k$ 
%   
can be used (i) to generate a spatially varying $\lambda$ value for frame $k$ 
(oppose to a fixed global value as
used in (\ref{equ:incPCP-g2})) to heavily penalize the high confidence background pixels,
(ii) to generate a ``new'' input frame $\hat{\mathbf{d}}_k^{(n)} = \mathbf{d}_k \odot \mathbf{\cal B}_k + 
\mathbf{l}_k^{(n)} \odot (\mathbf{1} - \mathbf{\cal B}_k)$, where  $\odot$
is element-wise multiplication (Hadamard product), in order to replace the effect of the previously 
processed original frame $\mathbf{d}_k$. 
% 
It is worth noting that the previously described actions, implicitly assume that a particular
incremental PCP algorithm has the ability to ``forget''  or ``replace'' the effect of a given frame
in the low-rank component, or has the ability to feedback an improved background estimate. 

As mentioned in section \ref{sec:incPCP}, 
% On what follows we describe the specific details on how to implement these ideas via 
% 
the incPCP algorithm \cite{rodriguez-2015-incremental}, makes use of 
rank-1 modifications for thin SVD, which includes the ``update'', ``replace''
and ``downdate'' (``forget'') operations (see \cite{brand-2006-fast} for details).
Based on such operations, in Algorithm \ref{algo:incPCP} we describe the specific details on how to implement
the above mentioned ideas for the incPCP algorithm, where
$[U_k^{(n)},\, \Sigma_k^{(n)},\, V_k^{(n)}]$ represents the SVD decomposition of the low-rank 
component as observed for the last $n$ frames, $\downdateSVD(.)$ and 
$\incrementalSVD(.)$ represent the ``downdate'' and ``update'' operations.

% \vspace{-1.5mm}
\IncMargin{-0.5em}
\begin{algorithm}[ht]
% \SetAlgoLined
\DontPrintSemicolon
% \SetKwInOut{Initialization}{Initialization}
\SetKwInOut{Compute}{Compute}
\SetKwInOut{Inputs}{Inputs}
 \Inputs{observed video frame $\mathbf{d}_k$, regularization parameter $\lambda$,
	 low-rank components $\mathbf{l}_k^{(n_1)}$ and $\mathbf{l}_k^{(n_2)}$,
	 sparse components $\mathbf{s}_k^{(n_1)}$ and $\mathbf{s}_k^{(n_2)}$,
	 scalar $\alpha > 1$.}
%  \KwResult{how to write algorithm with \LaTeX2e }
% 
\Compute{ considering $n = \{n_1,\, n_2\}$}{ 
% 
\nl $\mathbf{m}_k^{(n)} = \mbox{mask}( \mathbf{s}_k^{(n)}) \quad$ {\color{darkgray}(see comments in Section \ref{sec:rslt})}\; \\
% 
\nl $\mathbf{\cal B}_k = \; \sim\mathbf{m}_k^{(n_1)} \; \cup \; \sim\mathbf{m}_k^{(n_2)}$\; \\
% 
\nl $\hat{\mathbf{d}}_k^{(n)} = \mathbf{d}_k \odot \mathbf{\cal B}_k + 
\mathbf{l}_k^{(n)} \odot (\mathbf{1} - \mathbf{\cal B}_k)$\; \\
% 
\nl ${\bm\lambda}_{k} = \lambda \cdot (\mathbf{1} - \mathbf{\cal B}_k) + \alpha\cdot\lambda\cdot\mathbf{\cal B}_k 
 \quad$ {\color{darkgray}(spatially varying $\lambda$ value)}\\
% 
% \vspace{-2mm}
\nl $[U_k^{(n)}, \Sigma_k^{(n)}, V_k^{(n)}] = \downdateSVD(\mbox{``last col.''},
\, U_k^{(n)},\, \Sigma_k^{(n)},\, V_k^{(n)})$ \\
% 
\nl  $[U_k^{(n)}, \Sigma_k^{(n)}, V_k^{(n)}] = \incrementalSVD(\hat{\mathbf{d}}_k^{(n)}, 
U_{k}^{(n)}, \Sigma_{k}^{(n)}, V_{k}^{(n)})$\\
%
\nl $\mathbf{l}_k^{(n)} = U_k^{(n)} \cdot \Sigma_k^{(n)} \cdot {V_k^{(n)}(end,:)}^T$\\
\nl $\mathbf{s}_k^{(n)} = \shrink(\mathbf{d}_k - \mathbf{l}_k^{(n)}, {\bm\lambda}_{k})$
}
% 
% 
 \vspace{-1.0mm}
 \caption{Ghosting suppression incremental PCP (gs-incPCP).}
\label{algo:incPCP}
\end{algorithm}


% \footnotetext[7]{Computed via an automatic unimodal segmentation
% \cite{rosin-2001-unimodal} since the absolute value of the sparse
% representation has an unimodal histogram.
% }



% % If we assume that 
%  
% As mentioned before, the key idea towards our proposed ghost removal method is
% to simultaneously use two background estimates as derived
% from the previous N1 and N2 (N1 $\ll$ N2) to reduce the ghost
% effect.
% 
% background estimates differ from each other by ...
% 
% mask --> sparse estimate = sparse* + ghost + missing

 
\vspace{-1.0mm}
\section{Computational Results}
\label{sec:rslt}

We present F-measure based accuracy results (see Table \ref{tab:fm-accuracy})
for the I2R dataset\footnote[7]{The number of available ground-truth frames is 20 for all cases.} \cite{i2r-dataset} 
and from two challenging videos\footnote[8]{V320: $320 \times 240$ pixel, 1700-frame color video sequence,
with 1230 ground-truth frames,
from a highway camera with lots of cars passing by; V720: $720 \times 576$ pixel, 1200-frame color video sequence,
900 ground-truth frames,
of a train station with lots of people walking around.} 
from the CDnet dataset \cite{wang-2014-cdnet}.
The  F-measure, which makes use of a binary ground-truth, is defined as
%
\begin{eqnarray}
  F = \frac{2 \cdot P \cdot R}{P + R}, &   \displaystyle P = \frac{TP}{TP + FN}, &
   R = \frac{TP}{TP + FP}
  \label{eqn:Fm}
\end{eqnarray}
where P and R stands for precision and recall respectively, and TP, FN
and FP are the number of true positive, false negative and false
positive pixels, respectively.

In order to compute the F-measure for incPCP (original and ghosting suppression variants) as well as for GRASTA and
GOSUS, a threshold is needed to compute the binary foreground mask.
For the results presented in this section,  this threshold has
been computed via an automatic unimodal segmentation
\cite{rosin-2001-unimodal} since the absolute value of the sparse
representation has an unimodal histogram.
%
This approach, although simple,
adapts its threshold for each sparse representation and
ensures that all algorithms are fairly treated.

% \vspace{-4mm}
% \vspace{-5mm}
\begin{center}
\begin{table}[H]
{\small\centering
%
\addtolength{\tabcolsep}{-6.0pt}\renewcommand{\arraystretch}{0.815}
\begin{tabular}{c||c|c|c||c|c|c}

\multirow{4}{*}{Video} & \multicolumn{6}{c}{F-measure} \\ \cline{2-7}
%
  & \multicolumn{3}{c||}{grayscale} & \multicolumn{3}{c}{color}  \\ \cline{2-7}
%
  & \multirow{2}{*}{incPCP} & gs- & \multirow{2}{*}{GRASTA} & \multirow{2}{*}{incPCP} & gs- & \multirow{2}{*}{GOSUS}
 \\ 
%
 & & incPCP &  & & incPCP &  \\
 \hline\hline
%
{\footnotesize{Bootstrap  -- I2R}} & \multirow{3}{*}{0.587} & \multirow{3}{*}{\textbf{0.611}} & \multirow{3}{*}{0.608} &
\multirow{3}{*}{{0.636}}  & \multirow{3}{*}{\textbf{0.669}} & \multirow{3}{*}{0.659}\\
{\footnotesize{($120 \!\times\! 160 \!\times\! 3057$,}} & & & & & &\\
{\footnotesize{crowd scene)}}& & & & & &\\ \hline
%
{\footnotesize{Campus  -- I2R}} & \multirow{3}{*}{0.244} & \multirow{3}{*}{\textbf{0.771}}  & \multirow{3}{*}{0.215} &
\multirow{3}{*}{0.281} &
\multirow{3}{*}{\textbf{0.821}} & \multirow{3}{*}{0.166}  \\
{\footnotesize{($120 \!\times\! 160 \!\times\! 1439$,}} & & & & & &\\
{\footnotesize{waving trees)}}& & & & & & \\ \hline
%
{\footnotesize{Curtain  -- I2R}} & \multirow{3}{*}{{0.741}} & \multirow{2}{*}{0.757} & \multirow{3}{*}{\textbf{0.787}} &
\multirow{3}{*}{0.758} &
\multirow{2}{*}{0.783} & \multirow{3}{*}{\textbf{0.870}} \\
{\footnotesize{($120 \!\times\! 160 \!\times\! 2964$,}} & & & & & &\\
{\footnotesize{waving curtain)}}& & {\scriptsize{($n_1$=100)}}& & & {\scriptsize{($n_1$=100)}}&\\ \hline
%
{\footnotesize{Escalator  -- I2R}} & \multirow{3}{*}{0.481} & \multirow{3}{*}{\textbf{0.627}} & \multirow{3}{*}{0.539} &
\multirow{3}{*}{0.472} &
\multirow{3}{*}{\textbf{0.622}} & \multirow{3}{*}{0.405} \\
{\footnotesize{($130 \!\times\! 160 \!\times\! 3417$,}} & & & & & &\\
{\footnotesize{moving escalator)}}& & & & & &\\ \hline
%
{\footnotesize{Fountain  -- I2R}} & \multirow{3}{*}{{0.627}} & \multirow{3}{*}{\textbf{0.769}}  & \multirow{3}{*}{0.662} &
\multirow{3}{*}{0.632} &
\multirow{3}{*}{\textbf{0.789}} & \multirow{3}{*}{0.677} \\
{\footnotesize{($128 \!\times\! 160 \!\times\! 523$,}} & & & & & &\\
{\footnotesize{fountain water)}}& & & & & &\\ \hline
%
{\footnotesize{Hall  -- I2R}} & \multirow{3}{*}{0.570} & \multirow{3}{*}{{0.601}} & \multirow{3}{*}{\textbf{0.625}} &
\multirow{3}{*}{0.609} &
\multirow{3}{*}{\textbf{0.677}} & \multirow{3}{*}{0.464} \\
{\footnotesize{($144 \times 176 \times 3548$,}} & & & & & &\\
{\footnotesize{crowd scene)}}& & & & &\\ \hline
%
{\footnotesize{Lobby  -- I2R}} & \multirow{3}{*}{{0.550}} & \multirow{2}{*}{{0.466}} & \multirow{3}{*}{\textbf{0.567}} &
\multirow{3}{*}{\textbf{0.713}} &
\multirow{2}{*}{0.657} & \multirow{3}{*}{0.185} \\
{\footnotesize{($128 \!\times\! 160 \!\times\! 1546$,}} & & & & & &\\
{\footnotesize{switching light)}}& & {\scriptsize{($n_1$=150)}}& & & {\scriptsize{($n_1$=150)}}&\\ \hline
%
{\footnotesize{Mall  -- I2R}} & \multirow{3}{*}{{0.693}} & \multirow{3}{*}{\textbf{0.718}} & \multirow{3}{*}{0.692} &
\multirow{3}{*}{0.746} &
\multirow{3}{*}{\textbf{0.772}} & \multirow{3}{*}{0.715} \\
{\footnotesize{($256 \!\times\! 320 \!\times\! 1286$,}} & & & & & &\\
{\footnotesize{crowd scene)}}& & & & & &\\ \hline
%
{\footnotesize{WaterSurface -- I2R}}  & \multirow{3}{*}{0.636} & \multirow{3}{*}{\textbf{0.818}} & \multirow{3}{*}{0.772} &
\multirow{3}{*}{0.632} &
\multirow{3}{*}{\textbf{0.829}} & \multirow{3}{*}{0.787} \\
{\footnotesize{($128 \!\times\! 160 \!\times\! 633$,}} & & & & &\\
{\footnotesize{water surface)}}& & & & & &\\ \hline\hline
%
{\footnotesize{V320  -- CDnet}} & \multirow{3}{*}{0.745} & \multirow{3}{*}{\textbf{0.864}} & \multirow{3}{*}{{0.773}} &
\multirow{3}{*}{0.794} &
\multirow{3}{*}{\textbf{0.891}} & \multirow{3}{*}{0.549} \\
{\footnotesize{($320 \!\times\! 240 \!\times\! 1700$,}} & & & & &\\
{\footnotesize{highway camera)}}& & & & & &\\ \hline
% 
{\footnotesize{V720  -- CDnet}} & \multirow{3}{*}{0.687} & \multirow{3}{*}{\textbf{0.765}} & \multirow{3}{*}{0.169} &
\multirow{3}{*}{0.728} &
\multirow{3}{*}{\textbf{0.802}} & \multirow{3}{*}{0.426} \\
{\footnotesize{($720 \!\times\! 576 \!\times\! 1200$,}} & & & & &\\
{\footnotesize{train station)}}& & & & & &\\ \hline
% 
\end{tabular}}
%
\vspace{-2.5mm}
\caption{Accuracy performance via the F-measure on the I2R and CDnet datasets
  for the incPCP, original and ghosting suppression (gs-incPCP, Matlab code available~\cite{fastPCP-sim}) 
  variants, GRASTA and GOSUS algorithms.
  Below each video's name we include
  the size and total number of frames ($\mbox{rows} \times \mbox{columns} \times \mbox{frames}$)
  along with a short description.
   The bold values are the largest F-measure values (grayscale and color cases are treated independently).
}
\label{tab:fm-accuracy}
\end{table}
\end{center}
 
\vspace{-8.25mm}
Unless otherwise noted, the ghosting suppression incPCP algorithm uses\footnote[9]{As a rule of thumb $n_2 = 10 \times n_1$.
The value of $n_1$ depends on  video event's interpretation 
over a short time frame observation; for most of the considered test videos ``short time'' is about one second,
and thus $n_1=20$.} 
$n_1 = 20$, $n_2 = 200$ and $\alpha=2$ (see Algorithm \ref{algo:incPCP}).
Furthermore, for the GRASTA and GOSUS algorithms
we use their default batch procedure 
for the initial background estimation, since they
gives the best F-measure results.

The accuracy results
for the I2R and CDnet datasets, listed in Table \ref{tab:fm-accuracy},
show that the proposed method (gs-incPCP, Matlab code available~\cite{fastPCP-sim}) gives superior performance when 
compared to the considered alternatives. Moreover, gs-incPCP
is particularly effective when the analyzed video 
has an moving background, such as in the case of (i) the ``Campus''\footnote[10]{For the ``Campus'' test video, the 
gs-incPCP's
F-measure is largely better than those of all alternatives. In Fig. \ref{fig:campus-mask}
we depict some of the related results.}
% (see Fig. \ref{fig:campus-mask}) 
and ``V320'' videos, where waving trees/leaves are observed, (ii) the 
``WaterSurface'' video, where the background is mainly waving ocean,
(iii) the ``Fountain'' video, where a large fountain waterfall is
located behind a walkway, and (iv) the ``Escalator'' video, where
a large part of the scene is an escalator used by people passing by.

% Finally, we specially highlight the results for the ``Campus'' test video:

\begin{figure}[H]
\centering
  \begin{tabular}{cc}
%
    \hspace{-0.5mm}\subfigure[\protect\rule{0pt}{1.5em}Frame 813.]
    {\includegraphics[width=4.0cm]{campus_813}} &
% % % % %
    \hspace{-0.5mm}\subfigure[\protect\rule{0pt}{1.5em}Frame 832.]
    {\includegraphics[width=4.0cm]{campus-832}} \\
% 
    \hspace{-0.5mm}\subfigure[\protect\rule{0pt}{1.5em}gs-incPCP sparse component.]
    {\includegraphics[width=4.0cm]{S_incPCP__img0813}} &
% % % % %
    \hspace{-0.5mm}\subfigure[\protect\rule{0pt}{1.5em}gs-incPCP sparse component.]
    {\includegraphics[width=4.0cm]{S_incPCP__img0832}} \\
% 
    \hspace{-0.5mm}\subfigure[\protect\rule{0pt}{1.5em}Ground-truth binary mask.]
    {\includegraphics[width=4.0cm]{gt_campus_813}} &
% % % % %
    \hspace{-0.5mm}\subfigure[\protect\rule{0pt}{1.5em}Ground-truth binary mask.]
    {\includegraphics[width=4.0cm]{gt_campus-832}} \\
%     
    \hspace{-0.5mm}\subfigure[\protect\rule{0pt}{1.5em}Estimated binary mask.]
    {\includegraphics[width=4.0cm]{campusMask-incPCP_813}} &
% % % % %
    \hspace{-0.5mm}\subfigure[\protect\rule{0pt}{1.5em}Estimated binary mask.]
    {\includegraphics[width=4.0cm]{campusMask-incPCP_832}}

    \end{tabular}
%
  \vspace{-3.5mm}
  \caption{Original 813 (a) and 832 (b) frames of the ``Campus'' video, along with the corresponding 
  ground-truth binary masks (e)-(f) and
  estimated masks (g)-(h) and sparse component (c)-(d) via the gs-incPCP algorithm.}
 \label{fig:campus-mask}
%
\end{figure}


% \vspace{-1.5mm}
\section{Conclusions}
\label{sec:concl}

\vspace{-1.5mm}
The proposed method,
 ghosting suppression incremental PCP, 
%  \cmnt{move mention of code to previous section?} 
 is an effective
 algorithm that identifies and diminishes ghosting artifacts, including those in videos
 with a moving background, e.g. (i) waving trees and ocean, such as in the ``Campus'',  
 ``WaterSurface'' and
 ``V320'' test videos, (ii) for repetitive movement,
 such as in the ``Fountain'' and ``Escalator'' test videos.
 The proposed method gives superior quality and accuracy as determined by the F-measure
when estimating a binary foreground mask which  is computed via a simple global thresholding per frame.
% \mynote{something more ...} \cmnt{Comment on additional computational cost?}

% Compared to its original variant, the proposed method is approximately four to five times slower, 
The proposed method is  approximately four to five times slower than baseline incPCP, however
it can attain a processing frame rate throughput of 3 $\sim$ 10 f.p.s. for the considered
test videos on an Intel i7-4710HQ (2.5 GHz, 6MB Cache, 32GB RAM)
% quad-core (2.5 GHz, 6MB Cache, 32GB RAM)
based laptop.

\begin{spacing}{1.0}
% -------------------------------------------------------------------------
\bibliographystyle{IEEEbib}
\bibliography{jitterIncPCP}
\end{spacing}

\end{document}
